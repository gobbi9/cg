/**
 * Class that represents an abstract shape that contains a set of points, its fill color, border size and border color.
 */
class Shape {
	/**
	 * Constructor of Shape
	 * @param  {array} points - an array of 2,3 or 4-component arrays, example: `let shape = new Shape([[0.0,0.0],[1.0,0.0], [0.0,1.0]])`
	 * @param  {array} color - fill color of the shape (an array with 4 components in the format [red, green, blue, alpha]. Each of the values varies between 0 and 1 included). If the fill color is set as undefined or not given as parameter, only the polygon's outline will be drawn.
	 * @param  {float} borderSize - a number greater than zero, that represents the thickness of the border.
	 * @param  {array} borderColor - the border color (same type of color)
	 */
	constructor(points, color, borderSize = 0, borderColor = [0.3, 0.3, 0.3, 1.0]) {
		this.__points = points;
		this.verticeCount = points.length;
		this._points = new Float32Array(flattenPoints(points));
		this._color = color != undefined ? new Float32Array(color) : undefined;
		this.borderSize = borderSize;
		this._borderColor = new Float32Array(borderColor);
		this._updateType();
		this.pointSize = points && points[0] ? points[0].length : 0;
		this.groupId = -1;
		this.id = -1;
		this.partitionId = -1;
		this.borderId = -1;
	}

	/**
	 * @return {array} returns a point that represents the center of the shape
	 */
	centroid() {
		let center = [];
		for (let i = 0; i < this.pointSize; i++)
			center.push(0);

		for (let point of this.__points) {
			for (let i = 0; i < point.length; i++) {
				center[i] += point[i];
			}
		}

		for (let i = 0; i < center.length; i++) {
			center[i] /= this.verticeCount;
		}

		return center;
	}

	_updateType() {
		this.type = this._color == undefined ? STUB_CONTEXT.LINE_LOOP : STUB_CONTEXT.TRIANGLE_FAN;
	}

	/**
	 * @param  {array}  point - the point's coordinates
	 * @return {boolean}       returns whether point lies inside the shape or not (borders will not be considered).
	 */
	isPointInside(point) {
		let sumAngle = 0;
		let i = 0, j;
		for (let p1 of this.__points) {
			j = 0;
			for (let p2 of this.__points) {
				if (Math.abs(i-j) == 1) {
					let v1 = subtract(p1, point);
					let v2 = subtract(p2, point);
					sumAngle += Math.acos(dot(v1,v2)/(length(v1)*length(v2)));
				}
				j++;
			}
			i++;
		}
		return sumAngle >= 2*Math.PI;			

	}

	set points(points) {
		this.__points = points;
		this.verticeCount = points.length;
		this._points = new Float32Array(flattenPoints(points));
		this.pointSize = points && points[0] ? points[0].length : 0;
	}

	get points() {
		return this._points;
	}

	set color(color) {
		this._color = color != undefined ? new Float32Array(color) : undefined;
	}

	get color() {
		if (this.type == STUB_CONTEXT.TRIANGLE_FAN)
			return this._color;
		else
			return this._borderColor;
	}

	set borderColor(borderColor) {
		this._borderColor = new Float32Array(borderColor);
	}

	get borderColor() {
		return this._borderColor;
	}
}