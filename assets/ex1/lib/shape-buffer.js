/**
 * A class that represents a special array with objects of the class {@link Shape} of its subclasses.
 * All the shapes added to this buffer must have a dimension, i.e. all 2D or 3D shapes.
 */
class ShapeBuffer {

	/**
	 * Initializes an empty array called shapes that stores {@link Shape} objects.
	 */
	constructor() {
		this.clear();
	}

	/**
	 * @return {int} Returns of how many points each vertex of the shapes consist 
	 */
	numberOfVerticesPerShape() {
		if (this.shapes.length == 0)
			return 0;
		else
			return this.shapes[0].verticeCount;
	}

	/**
	 * @return {int} returns the dimension of the points in the shape.
	 */
	dimensionOfShape() {
		if (this.shapes.length == 0)
			return 0;
		else
			return this.shapes[0].pointSize;
	}

	/**
	 * @param {Shape} shape - Helper method to add a shape to the buffer
	 */
	push(shape) {
		shape.id = this.shapeCounter;
		shape.partitionId = this.partitions.push(shape.verticeCount) - 1;
		this.shapes.push(shape);
		let borderId;
		if (shape.borderSize > 0)
			borderId = this.borderShapes.push(new Shape(shape.__points, shape.borderColor));
		else
			borderId = this.borderShapes.push(new Shape(shape.__points, [0,0,0,0]));

		shape.borderId = borderId - 1;
		this.shapeCounter++;
	}

	/**
	 * Removes a shape from the buffer.
	 * @param  {Shape} shape
	 */
	remove(shape) {
		let shapesIdsToBeRemoved = [];
		let borderIdsToBeRemoved = [];
		let partitionsIdsToBeRemoved = [];
		if (shape.groupId > -1) {
			for (let s of this.shapes) {
				if (shape.groupId == s.groupId) {
					shapesIdsToBeRemoved.push(this.getIndexFromShape(s));
					borderIdsToBeRemoved.push(s.borderId);
					partitionsIdsToBeRemoved.push(s.partitionId);
				}
			}

			var sortDesc = (a, b) => {return b - a};
			shapesIdsToBeRemoved = shapesIdsToBeRemoved.sort(sortDesc);
			borderIdsToBeRemoved = borderIdsToBeRemoved.sort(sortDesc);
			partitionsIdsToBeRemoved = partitionsIdsToBeRemoved.sort(sortDesc);

			this.__remove_group(shapesIdsToBeRemoved, borderIdsToBeRemoved, partitionsIdsToBeRemoved);
		}
		else
			this.__remove(shape);



	}

	removeFromGroup(shape) {
		this.__remove(shape);
	}

	__remove(shape) {
		this.partitions.splice(shape.partitionId, 1);
		for (let s of this.shapes)
			if (s.partitionId > shape.partitionId)
				s.partitionId --;
		this.shapes.splice(this.getIndexFromShape(shape), 1);
		this.borderShapes.splice(shape.borderId, 1);
		for (let s of this.shapes)
			if (s.borderId > shape.borderId)
				s.borderId --;
	}

	__remove_group(shapesIds, partitionsIds, borderShapesIds) {
		for (let si of shapesIds)
			this.shapes.splice(si, 1);
		
		for (let bsi of borderShapesIds){
			for (let s of this.shapes)
				if (s.borderId > bsi)
					s.borderId --;
			this.borderShapes.splice(bsi, 1);

		}
		for (let p of partitionsIds){
			for (let s of this.shapes)
				if (s.partitionId > p)
					s.partitionId --;
			this.partitions.splice(p, 1);			
		}
	}
	

	/**
	 * Removes a shape from the buffer.
	 * @param  {int} shape's id.
	 */
	removeById(id) {
		this.remove(this.getShapeById(id));
	}

	/**
	 * @param  {int} id - shape's id
	 * @return {@link Shape}    the shape object in which shape.id == id
	 */
	getShapeById(id){
		for (let shape of this.shapes)
			if (shape.id == id)
				return shape;
		return null;
	}

	/**
	 * @param  {Shape} shape
	 * @return {int}  index of the shape in shapes array
	 */
	getIndexFromShape(shape) {
		for (let i = 0; i < this.shapes.length; i++)
			if (shape.id == this.shapes[i].id)
				return i;
		return -1;
	}


	/**
	 * Clears the buffer.
	 */
	clear(){
		this.shapes = [];
		this.borderShapes = [];
		this.partitions = [];
		this.groupCount = 0;
		this.shapeCounter = 0;
		this.tesselatedShapes = new Map();
	}

	/**
	 * Sets an unique id for the group, each shape belonging to the same group has the same groupId.
	 * @param  {array} shape indexes - an array of int (ids)
	 */
	group(...ids) {
		for (let id of ids)
			this.shapes[id].groupId = this.groupCount;

		this.groupCount++;
	}

	/**
	 * Unbinds the shapes from their corresponding groups.
	 * @param  {array} shape indexes - an array of int (ids)
	 */
	ungroup(...ids){
		for (let id of ids)
			this.shapes[id].groupId = -1;
	}

	/**
	 * @return {int} returns the size of the buffer
	 */
	size() {
		return this.shapes.length;
	}

	/**
	 * @return {int} the amount of border shapes
	 */
	borderShapesSize() {
		return this.borderShapes.length;
	}


	/**
	 * @return {array} Returns a flattened array consisting of all vertices of the shapes that are in the buffer.
	 */
	getAllVertices() {
		let vertices = [];

		for (let shape of this.shapes.concat(this.borderShapes))
			for (let coord of shape.points.values())
				vertices.push(coord);

		return new Float32Array(vertices);
	}

	/**
	 * @return {array} Returns a flattened array consisting of all colors of the shapes that are in the buffer.
	 */
	getAllColors() {
		let colors = [];

		for (let shape of this.shapes.concat(this.borderShapes))
			for (let i = 0; i < shape.verticeCount; i++) //one color per vertex
				for (let color of shape.color.values())
					colors.push(color);

		return new Float32Array(colors);
	}

	/**
	 * @param  {array} point - coordinates
	 * @return {int} returns the index of the first shape that has "point" inside.
	 */
	getSelectedShapeIndex(point) {
		for (let i = 0; i < this.shapes.length; i++)
			if (this.shapes[i].isPointInside(point))
				return i;

		return -1;
	}

	/**
	 * @param  {array} point - coordinates
	 * @return {int} returns the id of the first shape that has "point" inside.
	 */
	getSelectedShapeId(point) {
		for (let shape of this.shapes)
			if (shape.isPointInside(point))
				return shape.id;		

		return -1;
	}

	/**
	 * Returns the centroid of a group of shapes.
	 * @param  {int} groupId
	 * @return {array} the centroid
	 */
	groupCentroid(groupId){
		let centroids = [];
		for (let shape of this.shapes)
			if (shape.groupId == groupId)
				centroids.push(shape.centroid());

		let stubShape = new Shape(centroids);
		return stubShape.centroid();
	}

	/**
	 * Tesselates a shape.
	 * @param  {int} shapeId - the shape's id
	 */
	tesselate(shapeId, subDivisions = 1, middle = 0.5){
		if (!this.tesselatedShapes.has(shapeId)) {
			let shape = this.getShapeById(shapeId);
			this.tesselatedShapes.set(shapeId, shape.__points);

			let newShapes = [];
			let padding = 0;
			let tesselatedPoints = tesselate(shape.__points, subDivisions, middle).reverse();
			//console.log(tesselatedPoints);

			for (let i = 0; i < tesselatedPoints.length; i += 3) {
			 	let newShape = new Shape(tesselatedPoints.slice(i, i + 3),  shape.color, shape.borderSize, shape.borderColor);
			 	if (shape.groupId < 0)
			 		newShape.groupId = this.groupCount;
			 	else
			 		newShape.groupId = shape.groupId;
			 	newShapes.push(newShape);
			}

			if (shape.groupId < 0)
				this.groupCount++;
			
			for (let newShape of newShapes)
				this.push(newShape);
			if (shape.groupId < 0)
				this.remove(shape);
			else
				this.removeFromGroup(shape);
		}


	}

}