/**
 * @param  {array} points - a list of lists/points, example: [[1,2],[3,4]]
 * @return {array} - a flattened array, example: [1,2,3,4]
 */
function flattenPoints(points){
	let flattenedPoints = [];
	for (let point of points)
		for (let coord of point)
			flattenedPoints.push(coord);
	return flattenedPoints;
}

/**
 * A stub webgl context created to enable the access to constants, without having to pass the context as parameters to classes like {@link Shape}.
 */
var STUB_CONTEXT = document.createElement("canvas").getContext("webgl");

/**
 * @param  {array} point   
 * @param  {array} pointList
 * @return {array}   Given a point x and a list of points L where x is not in L, this function will return an array where x is the first element and the other elements are the two points closer to x.
 */
function getNeighbors(point, pointList) {
	let neighbors = [];
	neighbors.push(point);
	let pointListCopy = pointList.slice();
	pointListCopy.sort((a,b) => {
		return length(subtract(point, a)) - length(subtract(point, b));
	});

	neighbors.push(pointListCopy[0], pointListCopy[1]);
	return neighbors;
}

/**
 * @param  {array}  point
 * @param  {array}  listPoint
 * @return {boolean}  returns true if point is inside listPoint, false if otherwise.
 */
function hasPoint(point, listPoint){
	for (let p of listPoint){
		let i = 0;
		for (let coord of p) {
			if (coord != point[i])
				break;
			i++;
			if (i == p.length)
				return true;
		}
	}
	return false;
}

function tesselate(points, count = 1, middle = 0.5) {
	var outPoints = [];
	tesselate_rec(points, count, middle, outPoints);
	return outPoints;
}

/**
 * @param  {array} points - an array of coordinates
 * @param  {int} count - the depth of the recursion i.e. number of subdivisions
 * @param  {float} middle - a number between 0.0 and 1.0 indicating where in the lines the subdivision should occur. middle = 0.5 means in the center.
 * @param  {array} outPoints - array that stores all points of the resulting fractal
 */
function tesselate_rec(points, count, middle, outPoints) {
	// check for end of recursion
	if ( count === 0 ) {
		for (let point of points)
			//if (!hasPoint(point, outPoints))
				outPoints.push(point);
	}
	else {
		//let i = 0, j;
		let intersections = [];

		let i = 0;
		for (let point of points){
			let neighbors = getNeighbors(point, points.slice(0, i).concat(points.slice(i+1, points.length)));
			neighbors = neighbors.slice(1, neighbors.length);
			for (let neighbor of neighbors){
				let intersection = mix(point, neighbor, middle);
				if (!hasPoint(intersection, intersections))
					intersections.push(intersection);
			}
			i++;			
		}

		//console.log("intersections", intersections);

		//--count;
		for (let point of points)
			tesselate_rec(getNeighbors(point, intersections), count-1, middle, outPoints);
		//tesselate_rec(intersections, count-1, middle, outPoints);
	}
}

//console.log("tess", tesselate([[0.0,0.0], [1.0,0.0], [0.0, 1.0], [1.0,1.0]], 1, middle = 0.5));