var VSHADER_SOURCE = `
	attribute vec4 a_position;
	attribute vec4 a_color;

	varying vec4 v_color;

    uniform vec4 u_translation;
    uniform float u_theta;
    uniform vec4 u_center_rotation;
    uniform mat4 u_ortho_projection;
    uniform float u_aspect_ratio;

	void main() {
        vec2 moveToOrigin = vec2(0.0, 0.0);
        vec2 moveBack = vec2(0.0, 0.0);
        moveToOrigin.xy = - u_center_rotation.xy;
        moveBack.xy = - moveToOrigin.xy;

        float s = sin( u_theta );
        float c = cos( u_theta );

        vec4 projectedPosition = vec4(0.0,0.0,0.0,1.0);
        vec4 rotated_pos = vec4(0.0,0.0,0.0,1.0);
        vec4 rotated_transf = vec4(0.0,0.0,0.0,1.0);
        rotated_pos.xy = (a_position.xy + u_translation.xy + moveToOrigin.xy);


        rotated_transf.x = -s * rotated_pos.y + c * rotated_pos.x;
        rotated_transf.y = s * rotated_pos.x + c * rotated_pos.y;
        rotated_transf.z = 0.0;
        rotated_transf.w = 1.0;

        rotated_transf.xy = (rotated_transf.xy + moveBack.xy);

		projectedPosition.x = rotated_transf.x * (1.0 / u_aspect_ratio);
		projectedPosition.yzw = rotated_transf.yzw;
        //projectedPosition = projectedPosition + u_translation;

		gl_Position = projectedPosition * u_ortho_projection;

		//gl_PointSize = u_point_size;
		v_color = a_color;
	}
`;

var FSHADER_SOURCE = `
	precision highp float;

	varying vec4 v_color;

	void main(){
		gl_FragColor = v_color;
	}
`;

var aspectRatio = 16/9.0;

//rotation
var direction = 1, speed = 0.5;


var orthogonalProjection = ortho(-100, 100, -100, 100, -1, 1);
var inverseOrthogonalProjection = inverse4(orthogonalProjection);

var canvas, gl;

var bgColor = [.8, .8, .8, 1.0];

var currentVertex = 3; //white

var squareWithColors = new Float32Array([
    0.0, 0.0, 1.0, 0.0, 0.0, 1.0,       //x, y, red
    0.0, 40.0,  0.0, 1.0, 0.0, 1.0,     //x, y, green
    40.0, 40.0, 0.0, 0.0, 1.0, 1.0,     //x, y, blue
    40.0, 0.0, 1.0, 1.0, 1.0, 1.0       //x, y, white
]);

var lineSize = 6;
var displacement = [0,0];
var centerRotation = [squareWithColors[currentVertex * lineSize + 0], squareWithColors[currentVertex * lineSize + 1]];
var angle;

function main() {
    canvas = document.getElementById("main-canvas");
    gl = getWebGLContext(canvas, false);
    updateCanvasSize(gl);
    if (!gl) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    	console.log('Failed to initialize shaders.');
    	return;
    }

    initVertexColorBuffer(gl);

}

/**
 * Sets the canvas size and its aspect ration depending on the size of the user viewport.
 */
function updateCanvasSize (gl) {
	let percentage;
	aspectRatio = 16/9.0;
	if (window.innerWidth >= 1200)
	 	percentage = 0.6;
	else if (window.innerWidth > 768)
		percentage = 0.86;
	else if (window.innerWidth <= 768) {
		percentage = 0.80;
		aspectRatio = 1;
	}

	canvas.width = (window.innerWidth - 2) * percentage; // subtracting the borders
    canvas.height = canvas.width / aspectRatio;
    gl.viewport(0, 0, canvas.width, canvas.height)
}

function degrees(radians) {
    return (180.0 * radians) / Math.PI;
}

function initVertexColorBuffer(gl) {
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object ');
    }

    var FSIZE = squareWithColors.BYTES_PER_ELEMENT;

    // Bind the buffer object to a target
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    // Write date into the buffer object
    gl.bufferData(gl.ARRAY_BUFFER, squareWithColors, gl.STATIC_DRAW);

    var a_position = gl.getAttribLocation(gl.program, 'a_position');

    // Assign the buffer object to a_position variable
    gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, FSIZE * lineSize, 0);

    // Enable the assignment to a_position variable
    gl.enableVertexAttribArray(a_position);

    var a_color = gl.getAttribLocation(gl.program, 'a_color');

    // Assign the buffer object to a_color variable
    gl.vertexAttribPointer(a_color, 4, gl.FLOAT, false, FSIZE * lineSize, FSIZE * 2);

    // Enable the assignment to a_color variable
    gl.enableVertexAttribArray(a_color);
}


function draw(gl,
	translation = [0.0, 0.0],
	rotationAngle = 0) {

	/*var u_point_size = gl.getUniformLocation(gl.program, "u_point_size");
	gl.uniform1f(u_point_size, 10.0);*/

    /*var u_color = gl.getUniformLocation(gl.program, "u_color");
    gl.uniform4f(u_color, ...color);*/

    var u_theta = gl.getUniformLocation(gl.program, 'u_theta');
    gl.uniform1f(u_theta, rotationAngle);

    var u_aspect_ratio = gl.getUniformLocation(gl.program, 'u_aspect_ratio');
    gl.uniform1f(u_aspect_ratio, aspectRatio);

    var u_ortho_projection = gl.getUniformLocation(gl.program, "u_ortho_projection");
    gl.uniformMatrix4fv(u_ortho_projection, false, flatten(orthogonalProjection));

    var u_center_rotation = gl.getUniformLocation(gl.program, 'u_center_rotation');
    gl.uniform4f(u_center_rotation, ...centerRotation, 0.0, 1.0);

    var u_translation = gl.getUniformLocation(gl.program, 'u_translation');
    gl.uniform4f(u_translation, translation[0], translation[1], 0.0, 0.0);

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

}


function clear(gl, color) {
	gl.clearColor(...color);
    gl.clear(gl.COLOR_BUFFER_BIT);
}

/**
 * @param  canvas - the canvas DOM node
 * @param  {int} canvasX - the x coordinate inside the canvas
 * @param  {int} canvasY - the y coordinate inside the canvas
 * @return {array}       returns coordinates x and y between -1 and 1
 */
function transformCoordinates(canvas, canvasX, canvasY){
    let clipSpaceX, clipSpaceY;

    clipSpaceX = 2 * canvasX / canvas.width - 1;
    clipSpaceY = 1 - 2 * canvasY / canvas.height;

    return mult(mat4(clipSpaceX*aspectRatio, clipSpaceY, 0, 1), inverseOrthogonalProjection)[0].slice(0,2);
}

/**
 * Transforms the click coordinates into viewport coordinates.
 * @param  {clickEvent} e
 * @return {array}   returns coordinates x and y between -1 and 1
 */
function coordinatesClickEvent(e) {
    [canvasX, canvasY] = [e.clientX - canvas.getBoundingClientRect().left, e.clientY - canvas.getBoundingClientRect().top];
    return transformCoordinates(canvas, canvasX, canvasY);
}

/**
 * Transforms the touch coordinates into viewport coordinates.
 * @param  {touchEvent} e
 * @return {array}   returns coordinates x and y between -1 and 1
 */
function coordinatesTouchEvent(e) {
    let firstTouch = e.touches[0] != undefined ? e.touches[0] : e.changedTouches[0];
    [canvasX, canvasY] = [firstTouch.pageX - canvas.getBoundingClientRect().left, firstTouch.pageY - canvas.getBoundingClientRect().top];
    return transformCoordinates(canvas, canvasX, canvasY);
}

window.addEventListener("resize", e => {
	updateCanvasSize(gl);
});

window.addEventListener("load", e => {


	var then = 0;
	angle = 0;

	var idAnimationFrame = requestAnimationFrame(drawScene);

	function drawScene(now) {


		// Convert the time to seconds
		now *= 0.001;
		// Subtract the previous time from the current time
		var deltaTime = now - then;
		// Remember the current time for the next frame.
		then = now;

	    clear(gl, bgColor);
	    draw(gl, displacement, angle);
	    angle+=direction*deltaTime*(1 + speed)*(1 + speed)*(speed);

		/*if (Math.abs(i) > Math.PI) {
			shapeBuffer.ungroup(3);
		}*/

	    if (Math.abs(angle) > 2*Math.PI) {
	        //direction *= -1;
	        angle -= 2*Math.PI;
	        //shapeBuffer.group(0,1,2,3);
	    }

	    requestAnimationFrame(drawScene);


	}

    var x0, y0, xf, yf, dx, dy;

	main();

    $(canvas).on("mouseup touchstart", (evt) => {
        changeVertexRotation();
        evt.stopPropagation();
        evt.preventDefault();
    });

    function changeVertexRotation() {
        [xv, yv] = [squareWithColors[currentVertex * lineSize + 0], squareWithColors[currentVertex * lineSize + 1]];
        let originalVx = xv, originalVy = yv;
        xv += displacement[0];
        yv += displacement[1];
        currentVertex = (currentVertex + 1) % 4;
        [xv1, yv1] = [squareWithColors[currentVertex * lineSize + 0], squareWithColors[currentVertex * lineSize + 1]];
        xv1 += displacement[0];
        yv1 += displacement[1];
        let c = [xv1-xv, yv1-yv, 0.0, 1.0];
        let a = [0.0, 0.0, -1.0];//[xv,yv,-1];
        centerRotation = mult(mat4(...c), rotate(degrees(angle), a))[0].slice(0, 2);
        centerRotation[0] += xv;
        centerRotation[1] += yv;
        displacement = add(vec2(displacement), vec2(centerRotation[0] - xv1, centerRotation[1] - yv1));
    }

    //http://stackoverflow.com/a/17159809
    window.blockMenuHeaderScroll = false;
	$(window).on('touchstart', function(e)
	{
	    if ($(e.target).closest('#main-canvas').length == 1)
	    {
	        blockMenuHeaderScroll = true;
	    }
	});
	$(window).on('touchend', function()
	{
	    blockMenuHeaderScroll = false;
	});
	$(window).on('touchmove', function(e)
	{
	    if (blockMenuHeaderScroll)
	    {
	        e.preventDefault();
	    }
	});


});