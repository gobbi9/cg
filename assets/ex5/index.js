// standard global variables
var scene, camera, renderer, controls, stats;
var keyboard = new THREEx.KeyboardState();
var clock = new THREE.Clock();
var scope;
var listener;
var audioLoader;
var bounceHighAudio = [];
var bounceStopAudio = [];
// custom global variables
var cube;
var particleSystem, particleCount, particles;
var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
var bolas = [];
var velocidades = [];
var fallStatus = [];
var t_last = Date.now();
var bounce = [];
var arrayGiro = [];
var pararZ = [];
var velocidadeZ = [];
var reproduzirAudio = [];
var volume = [];
var ajuste = [];
var volumePivot = [];
var yAtual = 0;
var modo = [144,60,30];
var modoAtual = 0;
window.onkeydown = listenToKeyDown;

atualizarModo(freq);
init();
animate();


// FUNCTIONS 		
function init() 
{
	scope = this;
	// SCENE
	scene = new THREE.Scene();

	// CAMERA
	camera = setCamera(0,0,250);
	camera.rotation.order = "YXZ";

	// RENDERER
	renderer = initRenderer();
	
	//EVENTS
	autoRedimensionarJanela(true);
	
	// CONTROLS
	// controls = moverCamera(true);
	
	//Coloca um contador de FPS
	stats = adicionarStatus(true);

	luz = criaLuz();
	
	skybox = adicionarSkybox();
	
	plano();
	

	adicionarSkybox();
	adicionarAudioAmbiente(0.1);

	for(i = 1; i <= 7; i++){
		bolas.push(criaBolaRandom(20,i));
		bounce.push(0.8);
		criaAudio(1);
	}


	particulas = adicionarParticulas();

	with (camera.rotation) {
		x =	0;
		y =	0;
		z =	0;
	}
}

function renderParticles() {
	//particleSystem.position.y -= 0.5;
	//particleSystem.rotation.x -= 0.001;

	var pCount = particleCount;
	while (pCount--) {
		// get the particle
		
		var p = particleSystem.geometry.vertices[pCount];

		// check if we need to reset
		if (p.y < -100) {
		  p.setY(200);
		  p.velocity.setY(0);
		}

		// update the velocity with
		// a splat of randomniz
		p.velocity.setY( p.velocity.y - Math.random() * .01);

		// and the position
		p.setY(p.y + p.velocity.y);		
	}

	// flag to the particle system
	// that we've changed its vertices.
	particleSystem.geometry.verticesNeedUpdate = true;
}

function animate() 
{
	renderParticles();
    requestAnimationFrame( animate );
	render();		
	update();
	scope.bolas.forEach(quicar);
	scope.bolas.forEach(atritoCampo);

}

function update()
{
	moveCameraTeclado();
		
	if(controls != null)
		controls.update();
	if(stats != null)
		stats.update();
}

function render() 
{
	renderer.render( scene, camera );
}

function listenToKeyDown(e){
    if (e.ctrlKey){
    	atualizarModo(modo[modoAtual]);
		modoAtual++;
		modoAtual = modoAtual % 3;
    }
}

