
  var scene = new THREE.Scene();
  var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  scene.add(camera);
  var renderer = new THREE.WebGLRenderer({antialias:true});
  var controls;

  renderer.setClearColor(0xdddddd);
  renderer.setSize(window.innerWidth, window.innerHeight);
  
  //Ativando a sombra (pesquisar como faz)
  renderer.shadowMap.enabled = true;
  renderer.shadowMapSoft = true;
  
  // var cubeGeometry = new THREE.BoxGeometry(5, 5, 5);
  // var cubeMaterial = new THREE.MeshLambertMaterial({
  //   color: 0xff3300
  // });
  
  // var cube = new THREE.Mesh(cubeGeometry, cubeMaterial);

  // load a texture, set wrap mode to repeat
  var sphereTexture = new THREE.TextureLoader().load( "lib/img/chess.jpg" );
  sphereTexture.wrapS = THREE.RepeatWrapping;
  sphereTexture.wrapT = THREE.RepeatWrapping;
  sphereTexture.repeat.set( 1, 1 );
  var sphereMaterial = new THREE.MeshLambertMaterial( {
      map: sphereTexture
     } );
  var geometry = new THREE.SphereGeometry( 10, 32, 32 );
  var sphere = new THREE.Mesh( geometry, sphereMaterial );
  scene.add( sphere );
  
  sphere.position.y = 0;
  sphere.position.z = 3;
  sphere.position.x = 3;
  sphere.castShadow = true;

  // var planeGeometry = new THREE.PlaneGeometry (30,30,30);

  // // load a texture, set wrap mode to repeat
  // var texture = new THREE.TextureLoader().load( "lib/img/grasslight-big.jpg" );
  // texture.wrapS = THREE.RepeatWrapping;
  // texture.wrapT = THREE.RepeatWrapping;
  // texture.repeat.set( 6, 30 );
  // var planeMaterial = new THREE.MeshLambertMaterial( {
  //     map: texture
  //    } );
  // // var planeMaterial = new THREE.MeshLambertMaterial({color:0x00ff00});
  // var plane = new THREE.Mesh(planeGeometry, planeMaterial);
 
  // plane.rotation.x = -.5*Math.PI;
  // plane.receiveShadow = true;


   // scene.add(plane);

  // cube.position.y = 2.5;
  // cube.castShadow = true;

  // scene.add(cube);

  // // STATS
  // stats = new Stats();
  // stats.domElement.style.position = 'absolute';
  // stats.domElement.style.bottom = '0px';
  // stats.domElement.style.zIndex = 100;
  // document.body.append( stats.domElement );
  
  var light = new THREE.AmbientLight( 0x404040 ); // soft white light
  scene.add( light );

  var light = new THREE.HemisphereLight( 0xFFFFFF, 0xFFFFFF, 0.7 );
  scene.add( light );

  var spotLight = new THREE.SpotLight(0xFFFFFF)
  spotLight.castShadow = true;
  spotLight.position.set(15,50,50);
  spotLight.shadowMapWidth = 2048;
  spotLight.shadowMapHeight = 2048;

  scene.add(spotLight);

  camera.position.x = 0;
  camera.position.y = 0;
  camera.position.z = 40;

  camera.lookAt(scene.position);


  var imagePrefix = "lib/img/";
  var directions  = ["posx", "negx", "posy", "negy", "posz", "negz"];
  var imageSuffix = ".jpg";
  // var skyGeometry = new THREE.CubeGeometry( 5000, 5000, 5000 ); 
  
//skybox
  
 var imagePrefix = "lib/img/";
 var directions  = ["posx", "negx", "posy", "negy", "posz", "negz"];
 var imageSuffix = ".jpg";
   
 var materialArray = [];
 for (var i = 0; i < 6; i++)
  materialArray.push( new THREE.MeshBasicMaterial({
   map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
   side: THREE.BackSide
  }));
  
 var skyGeometry = new THREE.CubeGeometry( 500, 500, 500 );
 var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
 var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
 skyBox.receiveShadow = true;
 // skyBox.rotation.x += Math.PI / 2;
 scene.add( skyBox );

 controls = new THREE.OrbitControls( camera, renderer.domElement );
 


  document.body.append(renderer.domElement);

  function render() {
    requestAnimationFrame( render );
    renderer.render( scene, camera );
  }
  render();
  

