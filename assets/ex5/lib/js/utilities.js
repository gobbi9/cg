//Cria uma nova câmera
function setCamera(posX, posY, PosZ){
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
	camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
	scene.add(camera);
	camera.position.set(posX, posY, PosZ);
	camera.lookAt(scene.position);	
	//Corrige o chão do skybox
	camera.rotation.x = -0.00435499454676146;
	return camera;
}

//Inicia o renderer
function initRenderer(){
	if ( Detector.webgl )
		renderer = new THREE.WebGLRenderer( {antialias:true} );
	else
		renderer = new THREE.CanvasRenderer(); 
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	document.body.appendChild( renderer.domElement );
	renderer.setClearColor(0xdddddd);
	//Ativando a sombra
  	renderer.shadowMap.enabled = true;
	renderer.shadowMapSoft = true;
	return renderer;
}

function autoRedimensionarJanela(dimensionar){
	if(dimensionar === true){
		THREEx.WindowResize(renderer, camera);
		THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) });
	}
}

function moverCamera(move){
	// controls = null;
	if(move === true)
		controls = new THREE.OrbitControls( camera, renderer.domElement );
	return controls;
}

function adicionarStatus(bool){
	stats = null;
	if(bool === true){
		// STATS
		stats = new Stats();
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.bottom = '0px';
		stats.domElement.style.zIndex = 100;
		document.body.appendChild( stats.domElement );
	}
	return stats;
}

function adicionarSkybox(){
	//Skybox	
	var imagePrefix = "lib/img/";
	// var directions  = ["bg2", "bg2", "bg2-inv", "bg2", "bg2", "bg2"];
	var directions  = ["posx", "negx", "posy", "negy", "posz", "negz"];
	var imageSuffix = ".jpg";
	   
	var materialArray = [];
	for (var i = 0; i < 6; i++)
		materialArray.push( new THREE.MeshBasicMaterial({
		 	// map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
		 	map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),

		  	side: THREE.BackSide
		}));
	  
	var skyGeometry = new THREE.CubeGeometry( 5000, 5000, 5000 );
	var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
	var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
	skyBox.receiveShadow = true;
	// skyBox.rotation.x += Math.PI / 2;
	scene.add( skyBox );
	skyBox.position.y = 2450;

	return skyBox;
}

function plano(){
	var planeGeometry = new THREE.PlaneGeometry (5000,5000,5000);

	// load a texture, set wrap mode to repeat
	var texture = new THREE.TextureLoader().load( "lib/img/grasslight-big.jpg" );
	texture.wrapS = THREE.RepeatWrapping;
	texture.wrapT = THREE.RepeatWrapping;
	texture.repeat.set( 500, 250 );
	var planeMaterial = new THREE.MeshLambertMaterial( {
	    map: texture
	   } );
	// var planeMaterial = new THREE.MeshLambertMaterial({color:0x00ff00});
	var plane = new THREE.Mesh(planeGeometry, planeMaterial);
	plane.rotation.x = -.5*Math.PI;
	plane.position.y = -5;
	plane.receiveShadow = true;
	scene.add(plane);
}

function criaBola(raio,posX,posY,posZ){
	// load a texture, set wrap mode to repeat
	var sphereTexture = new THREE.TextureLoader().load( "lib/img/soccer.jpg" );
	sphereTexture.wrapS = THREE.RepeatWrapping;
	sphereTexture.wrapT = THREE.RepeatWrapping;
	sphereTexture.repeat.set( 1, 1 );
	var sphereMaterial = new THREE.MeshLambertMaterial( {
	    map: sphereTexture
	   } );
	var geometry = new THREE.SphereGeometry( raio, 100, 100 );
	var sphere = new THREE.Mesh( geometry, sphereMaterial );
	scene.add( sphere );
	velocidades.push(k_velocidade);
	fallStatus.push(true);
	  
	sphere.position.x = posX;
	sphere.position.y = posY;
	sphere.position.z = posZ;
	// sphere.rotation.y = 32.22;
	sphere.rotation.y = 34.48;

	sphere.castShadow = true;
	sphere.raio = raio;
	
	return sphere;
}

function criaBolaRandom(raio,i){
	// load a texture, set wrap mode to repeat
	var sphereTexture = new THREE.TextureLoader().load( "lib/img/soccer"+i+".jpg" );
	sphereTexture.wrapS = THREE.RepeatWrapping;
	sphereTexture.wrapT = THREE.RepeatWrapping;
	sphereTexture.repeat.set( 1, 1 );
	var sphereMaterial = new THREE.MeshLambertMaterial( {
	    map: sphereTexture
	   } );
	var geometry = new THREE.SphereGeometry( raio, 100, 100 );
	var sphere = new THREE.Mesh( geometry, sphereMaterial );
	scene.add( sphere );
	velocidades.push(k_velocidade);
	fallStatus.push(true);
	pararZ.push(false);
	velocidadeZ.push(k_velocidadeZ);
	reproduzirAudio.push(true);
	volume.push(1);
	
	  
	sphere.position.x = (Math.random() * 1000 - 700) % (100*(i+1));
	sphere.position.y = (Math.random() * 500 + 20) % (100*(i+1));
	sphere.position.z = (Math.random() * 50 - 500) % (100*(i+1));

	sphere.castShadow = true;
	sphere.raio = raio;
	
	return sphere;
}

function criaLuz(){
	// LIGHT
	var light = new THREE.PointLight(0xffffff);
	light.position.set(0,250,250);
	scene.add(light);
	light.castShadow = true;
	light.shadowMapWidth = 2048;
 	light.shadowMapHeight = 2048;


	var ambientLight = new THREE.AmbientLight( 0x777777 ); // soft white light
  	scene.add( ambientLight );

  	return light;
}

function moveCameraTeclado(){
	
	if ( keyboard.pressed("d") ) 
	{ 
		scope.camera.rotation.y -= k_botaoD;
	}
	if ( keyboard.pressed("a") ) 
	{ 
		scope.camera.rotation.y += k_botaoA;
	}
	if ( keyboard.pressed("w") ) 
	{ 
		if(scope.camera.rotation.x < 1.5) {
			scope.camera.rotation.x += k_botaoW2;
		}
	}

	if ( keyboard.pressed("f") ) 
	{ 
		arrayGiro.push(k_botaoF);
		arrayGiro.push('x');
		bolas.forEach(girar,arrayGiro);
		
	}
	if ( keyboard.pressed("r") ) 
	{ 
		arrayGiro.push(k_botaoR);
		arrayGiro.push('x');
		bolas.forEach(girar,arrayGiro);
		
	}
	if ( keyboard.pressed("s") ) 
	{ 
		//Valor mínimo de visualização da câmera
		if(scope.camera.position.y > -48){
			if(scope.camera.rotation.x > -1.55){
				scope.camera.rotation.x -= k_botaoS;
			}
		}
		
	}
	if(keyboard.pressed("1")){
		scope.camera.position.y = 0;
		scope.particulas.position.y = 0;
		yAtual = 0;
		
		scope.camera.position.x = 0;
		scope.particulas.position.x = 0;
		
		scope.camera.position.z = 250;
		scope.particulas.position.z = 250-250;
		
		scope.camera.rotation.x = 0;
		scope.particulas.rotation.x = 0;

		scope.camera.rotation.y = -0;
		scope.particulas.rotation.y = 0;

		scope.camera.rotation.z = 0;
		scope.particulas.rotation.z = 0;

	}
	if(keyboard.pressed("2")){
		scope.camera.position.y = 100;
		scope.particulas.position.y = 100;
		yAtual = 100;
		
		scope.camera.position.x = 1000;
		scope.particulas.position.x = 1000;
		
		scope.camera.position.z = 250;
		scope.particulas.position.z = 250-250;
		
		scope.camera.rotation.x = 0;
		scope.particulas.rotation.x = 0;

		scope.camera.rotation.y = -200;
		scope.particulas.rotation.y = 0;

		scope.camera.rotation.z = 0;
		scope.particulas.rotation.z = 0;

	}
	if(keyboard.pressed("3")){
		scope.camera.position.y = 2400;
		scope.particulas.position.y = 2400;
		yAtual = 2400;
		
		scope.camera.position.z = 0;
		scope.particulas.position.z = 0-250;
		
		scope.camera.position.x = 0;
		scope.particulas.position.x = 0;
		
		scope.camera.rotation.x = -1.55;
		scope.particulas.rotation.x = 0;

		scope.camera.rotation.y = 0;
		scope.particulas.rotation.y = 0;

		scope.camera.rotation.z = 0;
		scope.particulas.rotation.z = 0;


	}

	if(keyboard.pressed("4")){
		
		scope.camera.position.y = 700;
		scope.particulas.position.y = 700;
		yAtual = 700;
		
		scope.camera.position.z = 2000;
		scope.particulas.position.z = 2000-100;
		
		scope.camera.position.x = 0;
		scope.particulas.position.x = 0;
		
		scope.camera.rotation.x = -0.6;
		scope.particulas.rotation.x = -0.6;

		scope.camera.rotation.y = 0;
		scope.particulas.rotation.y = 0;

		scope.camera.rotation.z = 0;
		scope.particulas.rotation.z = 0;


	}

	if(keyboard.pressed("up")){
		
		let direction = new THREE.Vector3(0, 0, k_botaoUp);
		direction.applyEuler(scope.camera.rotation, scope.camera.rotation.order)
		scope.camera.position.add(direction);
		scope.particulas.position.add(direction);
		scope.camera.position.y = yAtual;
		scope.particulas.position.y = yAtual;

	}
	if(keyboard.pressed("down")){

		let direction = new THREE.Vector3(0, 0, -1);
		direction.applyEuler(scope.camera.rotation, scope.camera.rotation.order)
		scope.camera.position.sub(direction);
		scope.particulas.position.sub(direction);
		scope.camera.position.y = yAtual;
		scope.particulas.position.y = yAtual;
	}
	if(keyboard.pressed("left")){
		
		let direction = new THREE.Vector3(-1, 0, 0);
		direction.applyEuler(scope.camera.rotation, scope.camera.rotation.order)
		scope.camera.position.add(direction);
		scope.particulas.position.add(direction);
		scope.camera.position.y = yAtual;
		scope.particulas.position.y = yAtual;
		
	}
	if(keyboard.pressed("right")){
		let direction = new THREE.Vector3(1, 0, 0);
		direction.applyEuler(scope.camera.rotation, scope.camera.rotation.order)
		scope.camera.position.add(direction);
		scope.particulas.position.add(direction);
		scope.camera.position.y = yAtual;
		scope.particulas.position.y = yAtual;
	}
	if ( keyboard.pressed("space") ) 
	{ 
		velocidades.forEach(pular);
	}

}

function quicar(bola, index){

	if(velocidades[index] > k_quicar1 || bola.position.y > (0.0 +(bola.raio-5)) ){
		if(fallStatus[index] == true){
			velocidades[index] = velocidades[index] + (g * t);
			bola.position.y = bola.position.y - velocidades[index];

			//Quique da bola
			if(bola.position.y <= (0.0 +(bola.raio-5)) ){
				fallStatus[index] = false;
				velocidades[index] *= bounce[index];
				if(velocidades[index] > k_quicar2){
					bolas.forEach(ajustarVolume);
					volumePivot[index] = volume[index];
					volume[index] -= (volume[index]*ajuste[index]);
					bounceHighAudio[index].setVolume(volume[index]);
					bounceHighAudio[index].play();
					volume[index] += (volumePivot[index]*ajuste[index]);

				}
				else{
					volume[index] -= 0.1;
					if(volume[index] < 0 ){
						volume[index] = 0;
					}
					
					if(reproduzirAudio[index] == true){
						bolas.forEach(ajustarVolume);
						volumePivot[index] = volume[index];
						volume[index] -= (volume[index]*ajuste[index]);
						bounceStopAudio[index].setVolume(volume[index]);
						bounceStopAudio[index].play();
						volume[index] += (volumePivot[index]*ajuste[index]);
						reproduzirAudio[index] = false;
					}	
				}		
			}
		}
		else{
			bola.position.y = bola.position.y + velocidades[index];
			velocidades[index] = velocidades[index] - (g * t);
			if(velocidades[index] <= 0 ){
				fallStatus[index] = true;
			}
		}
	}
	if(velocidades[index] < k_quicar3 && bola.position.y <= (0.001 +(bola.raio-5))){
		bola.position.y = (0.0 +(bola.raio-5));
		velocidades[index] = 0;
		fallStatus[index] = false;
		pararZ[index] = true;
	}
	else{
		pararZ[index] = false;
	}
	
	mudarVelocidadeZ(bola,velocidadeZ[index], velocidadeZ, index);

	if(pararZ[index] && velocidadeZ[index] > 0){
		velocidadeZ[index] -= k_quicar4;
		if(velocidadeZ[index] <= k_quicar5){
			velocidadeZ[index] = 0;
		}
	}else if(pararZ[index] && velocidadeZ[index] < 0){

		velocidadeZ[index] += k_quicar6;
		if(velocidadeZ[index] >= k_quicar7){
			velocidadeZ[index] = 0;
		}
	}


}

function atritoCampo(bola,index){
	if(bola.position.y <= (0.001 +(bola.raio-5)) && velocidadeZ[index] > 0){
		velocidadeZ[index] -= k_quicar4;
		if(velocidadeZ[index] <= k_quicar5){
			velocidadeZ[index] = 0;
		}
	}else if(bola.position.y <= (0.001 +(bola.raio-5)) && velocidadeZ[index] < 0){

		velocidadeZ[index] += k_quicar4;
		if(velocidadeZ[index] >= k_quicar7){
			velocidadeZ[index] = 0;
		}
	}

}

function pular(velocidade, index){
	velocidades[index] += (Math.floor((Math.random() * 10) + 1))*k_pular3;
	if(velocidades[index] < k_pular1){
		velocidades[index] = k_pular2;
	}
	reproduzirAudio[index] = true;
	volume[index] = 1;
}

function girar(bola, index){
	if(this[1] == 'y'){
		scope.bolas[index].rotation.y += this[0];

	}
	else if (this[1] == 'x'){
		scope.bolas[index].rotation.x += this[0];
		scope.velocidadeZ[index] += this[0];
	}
	else if (this[1] == 'z'){
		scope.bolas[index].rotation.z += this[0];
	}
	arrayGiro = [];
}

function mudarVelocidadeZ(bola,velocidade, velocidadeZ, index){
	bola.rotation.x += velocidade;
	bola.position.z += 30*velocidade;
	if(bola.position.z >= 2490){

		ajustarVolume(bola, index);
		volumePivot[index] = volume[index];
		volume[index] -= (volume[index]*ajuste[index]);
		bounceHighAudio[index].setVolume(volume[index]);
		bounceHighAudio[index].play();
		volume[index] += (volumePivot[index]*ajuste[index]);
		velocidadeZ[index] = -(velocidadeZ[index]);

	}else if(bola.position.z <= -2490){

		ajustarVolume(bola, index);
		volumePivot[index] = volume[index];
		volume[index] -= (volume[index]*ajuste[index]);
		bounceHighAudio[index].setVolume(volume[index]);
		bounceHighAudio[index].play();
		volume[index] += (volumePivot[index]*ajuste[index]);
		velocidadeZ[index] = -(velocidadeZ[index]);
	}

}


function adicionarParticulas() {
	// create the particle variables
	particleCount = 3000;
	particles = new THREE.Geometry();
	var pMaterial = new THREE.PointsMaterial({
			color: 0xFFFFFF,
			size: 2.5,
			map: THREE.ImageUtils.loadTexture("lib/img/raindrop-2.png"),
			blending: THREE.AdditiveBlending,
			transparent: true
		});

	// now create the individual particles
	for (var p = 0; p < particleCount; p++) {
	  // create a particle with random
	  // position values, -250 -> 250
	  var pX = Math.random() * 500 - 250,
	      pY = Math.random() * 500,
	      pZ = Math.random() * 500;
	   var particle = {};
	      particle = new THREE.Vector3(pX, pY, pZ);
		  particle.velocity = new THREE.Vector3(
		    0,              // x
		    -Math.random()*k_particulas, // y: random vel
		    0);				// z
	  // add it to the geometry
	  particles.vertices.push(particle);
	}


	// create the particle system
	particleSystem = new THREE.Points(
	    particles,
	    pMaterial);

	// also update the particle system to
	// sort the particles which enables
	// the behaviour we want
	particleSystem.sortParticles = false;

	// add it to the scene
	scene.add(particleSystem);
	return particleSystem;

}

function adicionarAudioAmbiente(volume){
	// global ambient audio
	listener = new THREE.AudioListener();
	camera.add( listener );
	audioLoader = new THREE.AudioLoader();
	var sound4 = new THREE.Audio( listener );
	audioLoader.load( 'lib/sounds/rain.ogg', function( buffer ) {
		sound4.setBuffer( buffer );
		sound4.setLoop(true);
		sound4.setVolume(volume);
		sound4.play();
	});
}

function criaAudio(volume){
	var bouceHigh = new THREE.Audio( listener );
	audioLoader.load( 'lib/sounds/bounce-high.mp3', function( buffer ) {
		bouceHigh.setBuffer( buffer );
		bouceHigh.setLoop(false);
		bouceHigh.setVolume(volume);
	});

	bounceHighAudio.push(bouceHigh);

	var bounceStop = new THREE.Audio( listener );
	audioLoader.load( 'lib/sounds/bounce-stop.ogg', function( buffer ) {
		bounceStop.setBuffer( buffer );
		bounceStop.setLoop(false);
		bounceStop.setVolume(volume);
	});

	bounceStopAudio.push(bounceStop);

}

function ajustarVolume(bola,index){
	var result = Math.abs(distanciaBola(bola)/2500);
	if(result < 1)
		ajuste[index] = result;
	else
		ajuste[index] = 1;
}

function distanciaBola(bola){
	
	return ((bola.position.x-scope.camera.position.x)^2)+((bola.position.z-scope.camera.position.z)^2);
}