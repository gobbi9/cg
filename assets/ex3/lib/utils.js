/**
 * Generates a list with the 8 vertices of a cube centered on (0,0,0) with size "size".
 * @param  {float} size - the size of each side of the cube.
 * @return {array}  the ordered vertices
 */
function generateCubeVertices(size) {
	let side = size/2.0;
	let vertices = [];
	vertices.push([-side, side, side]);
	vertices.push([-side, side, -side]);
	vertices.push([side, side, -side]);
	vertices.push([side, side, side]);
	vertices.push([side, -side, side]);
	vertices.push([side, -side, -side]);
	vertices.push([-side, -side, -side]);
	vertices.push([-side, -side, side]);
	return vertices;
}