var VSHADER_SOURCE = `
	attribute vec4 a_position;
	attribute vec4 a_color;
    attribute vec4 a_center_rotation;

	varying vec4 v_color;

    uniform vec4 u_translation;    
    uniform mat4 u_ortho_projection;
    uniform float u_aspect_ratio;

    uniform mat4 u_perspective;
    uniform mat4 u_camera_perspective;
    uniform mat4 u_camera_orthogonal;
    uniform mat4 u_rotation_x;
    uniform mat4 u_rotation_y;
    uniform mat4 u_rotation_z;

    uniform float u_is_perspective;

	void main() {
        vec3 moveToOrigin = vec3(0.0, 0.0, 0.0);
        vec3 moveBack = vec3(0.0, 0.0, 0.0);
        moveToOrigin.xyz = - a_center_rotation.xyz;
        moveBack.xyz = - moveToOrigin.xyz;

        vec4 projectedPosition = vec4(0.0,0.0,0.0,1.0);
        vec4 rotated_pos = vec4(0.0,0.0,0.0,1.0);
        vec4 rotated_transf = vec4(0.0,0.0,0.0,1.0);
        rotated_pos.xyz = (a_position.xyz + moveToOrigin.xyz);
        rotated_pos.w = a_position.w;

        rotated_transf = u_rotation_x * u_rotation_y * u_rotation_z * rotated_pos;

        rotated_transf.xyz = (rotated_transf.xyz + moveBack.xyz);

        projectedPosition = rotated_transf;
        projectedPosition = projectedPosition + u_translation;

        if (u_is_perspective >= 1.0)
            gl_Position =  u_perspective * u_camera_perspective * projectedPosition;
        else 
            gl_Position =  u_ortho_projection * u_camera_orthogonal * projectedPosition;

		//gl_PointSize = u_point_size;
		v_color = a_color;
	}
`;

var FSHADER_SOURCE = `
	precision highp float;

	varying vec4 v_color;

	void main(){
		gl_FragColor = v_color;
	}
`;

var aspectRatio = 16/9.0;

//rotation
var direction = 0, speed = 0.5;

var orthoX = 100, orthoY = 100, orthoZ = 1000;
var orthogonalProjection = ortho(-orthoX * aspectRatio, orthoX * aspectRatio, -orthoY, orthoY, -orthoZ, orthoZ);
var inverseOrthogonalProjection = inverse4(orthogonalProjection);

var perspectiveMatrix = perspective(40, aspectRatio, 60, -60);
var camera_perspective = lookAt(vec3(160, 160, 160), vec3(-1, -1, -1), vec3(-1/2, -1/2, 1));
var camera_orthogonal = lookAt(vec3(1, 1, 1), vec3(-1, -1, -1), vec3(-1/2, -1/2, 1));
var isPerspective = 1.0; //true
var rotationX = mat4(),
    rotationY = mat4(),
    rotationZ = mat4();
var angleX = 0, angleY = 0, angleZ = 0;
var x_axis = false, y_axis = false, z_axis = true;

var canvas, gl;

var bgColor = [.8, .8, .8, 1.0];
var cubeSize = 60.0;

let vertices = generateCubeVertices(cubeSize);

let colorFaces = [
    [1.0, 0.0, 0.0, 1.0], //red
    [0.0, 1.0, 0.0, 1.0], //green
    [0.0, 0.0, 1.0, 1.0], //blue
    [1.0, 1.0, 0.0, 1.0], //yellow
    [1.0, 0.0, 1.0, 1.0], //pink
    [0.0, 1.0, 1.0, 1.0]  //cyan
];

let centroid = [0, 0, 0];

//gl.TRIANGLE_FAN
let cubeVerticesWithFaceColors = new Float32Array([
    //face 1 (2)
    ...vertices[0], ...colorFaces[1], ...centroid,
    ...vertices[1], ...colorFaces[1], ...centroid,
    ...vertices[2], ...colorFaces[1], ...centroid,
    ...vertices[3], ...colorFaces[1], ...centroid,
    //face 2 (1)
    ...vertices[4], ...colorFaces[4], ...centroid,
    ...vertices[5], ...colorFaces[4], ...centroid,
    ...vertices[6], ...colorFaces[4], ...centroid,
    ...vertices[7], ...colorFaces[4], ...centroid,
    //face 3 (4)
    ...vertices[0], ...colorFaces[2], ...centroid,
    ...vertices[3], ...colorFaces[2], ...centroid,
    ...vertices[4], ...colorFaces[2], ...centroid,
    ...vertices[7], ...colorFaces[2], ...centroid,
    //face 4 (3)
    ...vertices[1], ...colorFaces[3], ...centroid,
    ...vertices[2], ...colorFaces[3], ...centroid,
    ...vertices[5], ...colorFaces[3], ...centroid,
    ...vertices[6], ...colorFaces[3], ...centroid,
    //face 5 (6)
    ...vertices[2], ...colorFaces[0], ...centroid,
    ...vertices[3], ...colorFaces[0], ...centroid,
    ...vertices[4], ...colorFaces[0], ...centroid,
    ...vertices[5], ...colorFaces[0], ...centroid,
    //face 6 (5)
    ...vertices[0], ...colorFaces[5], ...centroid,
    ...vertices[1], ...colorFaces[5], ...centroid,
    ...vertices[6], ...colorFaces[5], ...centroid,
    ...vertices[7], ...colorFaces[5], ...centroid
]);

let edgeColor = [0.0,0.0,0.0,1.0];

//gl.LINES
let cubeEdges = new Float32Array([
    ...vertices[0], ...edgeColor, ...centroid,
    ...vertices[1], ...edgeColor, ...centroid,
    ...vertices[1], ...edgeColor, ...centroid,
    ...vertices[2], ...edgeColor, ...centroid,
    ...vertices[2], ...edgeColor, ...centroid,
    ...vertices[3], ...edgeColor, ...centroid,
    ...vertices[0], ...edgeColor, ...centroid,
    ...vertices[3], ...edgeColor, ...centroid,
    ...vertices[0], ...edgeColor, ...centroid,
    ...vertices[7], ...edgeColor, ...centroid,
    ...vertices[1], ...edgeColor, ...centroid,
    ...vertices[6], ...edgeColor, ...centroid,
    ...vertices[3], ...edgeColor, ...centroid,
    ...vertices[4], ...edgeColor, ...centroid,
    ...vertices[2], ...edgeColor, ...centroid,
    ...vertices[5], ...edgeColor, ...centroid,
    ...vertices[6], ...edgeColor, ...centroid,
    ...vertices[7], ...edgeColor, ...centroid,
    ...vertices[5], ...edgeColor, ...centroid,
    ...vertices[6], ...edgeColor, ...centroid,
    ...vertices[4], ...edgeColor, ...centroid,
    ...vertices[5], ...edgeColor, ...centroid,
    ...vertices[4], ...edgeColor, ...centroid,
    ...vertices[7], ...edgeColor, ...centroid    
]);

var floor = (cubeSize / 2) * Math.sqrt(2);
var road = new Float32Array([
    -40, -1000, -floor, ...edgeColor, -40, -1000, -floor,
    -40,  1000, -floor, ...edgeColor, -40,  1000, -floor,
    40, -1000, -floor, ...edgeColor, 40, -1000, -floor,
    40,  1000, -floor, ...edgeColor, 40,  1000, -floor
]);

var roadSize = 4;

var axes = new Float32Array([
    0, 0, 0, ...edgeColor, ...centroid,
    cubeSize * 1.5, 0, 0, ...colorFaces[0], ...centroid,
    0, 0, 0, ...edgeColor, ...centroid,
    0, cubeSize * 1.5, 0, ...colorFaces[1], ...centroid,
    0, 0, 0, 1,1,1,1, ...centroid,
    0, 0, cubeSize * 1.5, ...colorFaces[2], ...centroid,

    0, 0, 0, ...edgeColor, ...centroid,
    cubeSize * 2, 0, 0, ...edgeColor, cubeSize * 2, 0, 0,
    0, 0, 0, ...edgeColor, ...centroid,
    0, cubeSize * 2, 0, ...edgeColor, 0, cubeSize * 2, 0,
    0, 0, 0, 1,1,1,1, ...centroid,
    0, 0, cubeSize * 2, ...edgeColor, 0, 0, cubeSize * 2  
]);

var axesSize = 12;

var cubeBuffer = new Float32Array([...cubeVerticesWithFaceColors, ...cubeEdges, ...road, ...axes]);
cubeBuffer.lineSize = 10;
cubeBuffer.faces = 6;

var displacement = [0,0,0];
var centerRotation = [0,0,0]
var angle = 0, dAngleX = 0, dAngleY = 0, dAngleZ = 0;

function main() {
    canvas = document.getElementById("main-canvas");
    gl = getWebGLContext(canvas, false);
    updateCanvasSize(gl);
    if (!gl) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    	console.log('Failed to initialize shaders.');
    	return;
    }

    gl.enable(gl.DEPTH_TEST);
    initVertexColorBuffer(gl);

}

/**
 * Sets the canvas size and its aspect ration depending on the size of the user viewport.
 */
function updateCanvasSize (gl) {
	let percentage;
	aspectRatio = 16/9.0;
	if (window.innerWidth >= 1200)
	 	percentage = 0.6;
	else if (window.innerWidth > 768)
		percentage = 0.86;
	else if (window.innerWidth <= 768) {
		percentage = 0.80;
		aspectRatio = 1;
	}

    orthogonalProjection = ortho(-orthoX * aspectRatio, orthoX * aspectRatio, -orthoY, orthoY, -orthoZ, orthoZ);
    perspectiveMatrix = perspective(40, aspectRatio, 60, -60);
	canvas.width = (window.innerWidth - 2) * percentage; // subtracting the borders
    canvas.height = canvas.width / aspectRatio;
    gl.viewport(0, 0, canvas.width, canvas.height);
    $("#form-container").css("width", percentage * 100 + "%");
}

function degrees(radians) {
    return (180.0 * radians) / Math.PI;
}

function initVertexColorBuffer(gl) {
    var buffer = gl.createBuffer();
    if (!buffer) {
        console.log('Failed to create the buffer object ');
    }

    var FSIZE = cubeBuffer.BYTES_PER_ELEMENT;

    // Bind the buffer object to a target
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    // Write date into the buffer object
    gl.bufferData(gl.ARRAY_BUFFER, cubeBuffer, gl.STATIC_DRAW);

    var a_position = gl.getAttribLocation(gl.program, 'a_position');

    // Assign the buffer object to a_position variable
    gl.vertexAttribPointer(a_position, 3, gl.FLOAT, false, FSIZE * cubeBuffer.lineSize, 0);

    // Enable the assignment to a_position variable
    gl.enableVertexAttribArray(a_position);

    var a_color = gl.getAttribLocation(gl.program, 'a_color');

    // Assign the buffer object to a_color variable
    gl.vertexAttribPointer(a_color, 4, gl.FLOAT, false, FSIZE * cubeBuffer.lineSize, FSIZE * 3);

    // Enable the assignment to a_color variable
    gl.enableVertexAttribArray(a_color);

    var a_center_rotation = gl.getAttribLocation(gl.program, 'a_center_rotation');

    gl.vertexAttribPointer(a_center_rotation, 3, gl.FLOAT, false, FSIZE * cubeBuffer.lineSize, FSIZE * 7);
    gl.enableVertexAttribArray(a_center_rotation);

}


function draw(gl,
	translation = [0.0, 0.0, 0.0],
	rotationAngle = 0) {   
    

    if (x_axis)
        rotationX = rotate(degrees(dAngleX + angle), [1,0,0]);
    else
        rotationX = rotate(degrees(dAngleX), [1,0,0]);

    if (y_axis)
        rotationY = rotate(degrees(dAngleY + angle), [0,1,0]);
    else
        rotationY = rotate(degrees(dAngleY), [0,1,0]);

    if (z_axis)
        rotationZ = rotate(degrees(dAngleZ + angle), [0,0,1]);
    else
        rotationZ = rotate(degrees(dAngleZ), [0,0,1]);

    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_rotation_x"), false, flatten(rotationX));
    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_rotation_y"), false, flatten(rotationY));
    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_rotation_z"), false, flatten(rotationZ));

    var u_aspect_ratio = gl.getUniformLocation(gl.program, 'u_aspect_ratio');
    gl.uniform1f(u_aspect_ratio, aspectRatio);

    var u_ortho_projection = gl.getUniformLocation(gl.program, "u_ortho_projection");
    gl.uniformMatrix4fv(u_ortho_projection, false, flatten(orthogonalProjection));

    // var u_center_rotation = gl.getUniformLocation(gl.program, 'u_center_rotation');
    // gl.uniform4f(u_center_rotation, ...centerRotation, 1.0);

    var u_translation = gl.getUniformLocation(gl.program, 'u_translation');
    gl.uniform4f(u_translation, translation[0], translation[1], translation[2], 0.0);

    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_perspective"), false, flatten(perspectiveMatrix));
    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_camera_perspective"), false, flatten(camera_perspective));
    gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, "u_camera_orthogonal"), false, flatten(camera_orthogonal));

    gl.uniform1f(gl.getUniformLocation(gl.program, "u_is_perspective"), isPerspective);

    let i;
    for (i = 0; i < cubeBuffer.faces * 4; i += 4)
        gl.drawArrays(gl.TRIANGLE_FAN, i, 4);

    for (let j = i; j < i + cubeBuffer.faces * 4 + roadSize + axesSize; j += 2)
        gl.drawArrays(gl.LINES, j, 2);


}


function clear(gl, color) {
	gl.clearColor(...color);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
}

/**
 * @param  canvas - the canvas DOM node
 * @param  {int} canvasX - the x coordinate inside the canvas
 * @param  {int} canvasY - the y coordinate inside the canvas
 * @return {array}       returns coordinates x and y between -1 and 1
 */
function transformCoordinates(canvas, canvasX, canvasY){
    let clipSpaceX, clipSpaceY;

    clipSpaceX = 2 * canvasX / canvas.width - 1;
    clipSpaceY = 1 - 2 * canvasY / canvas.height;

    return mult(mat4(clipSpaceX*aspectRatio, clipSpaceY, 0, 1), inverseOrthogonalProjection)[0].slice(0,2);
}

/**
 * Transforms the click coordinates into viewport coordinates.
 * @param  {clickEvent} e
 * @return {array}   returns coordinates x and y between -1 and 1
 */
function coordinatesClickEvent(e) {
    [canvasX, canvasY] = [e.clientX - canvas.getBoundingClientRect().left, e.clientY - canvas.getBoundingClientRect().top];
    return transformCoordinates(canvas, canvasX, canvasY);
}

/**
 * Transforms the touch coordinates into viewport coordinates.
 * @param  {touchEvent} e
 * @return {array}   returns coordinates x and y between -1 and 1
 */
function coordinatesTouchEvent(e) {
    let firstTouch = e.touches[0] != undefined ? e.touches[0] : e.changedTouches[0];
    [canvasX, canvasY] = [firstTouch.pageX - canvas.getBoundingClientRect().left, firstTouch.pageY - canvas.getBoundingClientRect().top];
    return transformCoordinates(canvas, canvasX, canvasY);
}

window.addEventListener("resize", e => {
	updateCanvasSize(gl);
});

window.addEventListener("load", e => {


	var then = 0;
	angle = 0;

	var idAnimationFrame = requestAnimationFrame(drawScene);

	function drawScene(now) {


		// Convert the time to seconds
		now *= 0.001;
		// Subtract the previous time from the current time
		var deltaTime = now - then;
		// Remember the current time for the next frame.
		then = now;

	    clear(gl, bgColor);
	    draw(gl, displacement, angle);
        if (x_axis || y_axis || z_axis)
	       angle+=direction*deltaTime*(1 + speed)*(1 + speed)*(speed);

		/*if (Math.abs(i) > Math.PI) {
			shapeBuffer.ungroup(3);
		}*/

	    if (Math.abs(angle) > 2*Math.PI) {
	        //direction *= -1;
	        angle -= 2*Math.PI;
	        //shapeBuffer.group(0,1,2,3);
	    }

	    requestAnimationFrame(drawScene);


	}

    var x0, y0, xf, yf, dx, dy;

	main();

    $(canvas).on("mouseup touchstart", (evt) => {
        evt.stopPropagation();
        evt.preventDefault();
    });


    //http://stackoverflow.com/a/17159809
    window.blockMenuHeaderScroll = false;
	$(window).on('touchstart', function(e)
	{
	    if ($(e.target).closest('#main-canvas').length == 1)
	    {
	        blockMenuHeaderScroll = true;
	    }
	});
	$(window).on('touchend', function()
	{
	    blockMenuHeaderScroll = false;
	});
	$(window).on('touchmove', function(e)
	{
	    if (blockMenuHeaderScroll)
	    {
	        e.preventDefault();
	    }
	});


    //Controls
    function activateRotation() {
        let checkBox = $("#rotation")[0];
        $("#ccw").prop("disabled", !checkBox.checked);
        $("#cw").prop("disabled", !checkBox.checked);
        $("#speed_range").prop("disabled", !checkBox.checked);
        $("#x_axis").prop("disabled", !checkBox.checked);
        $("#y_axis").prop("disabled", !checkBox.checked);
        $("#z_axis").prop("disabled", !checkBox.checked);

        //dAxis = vec3(axis);
        if (x_axis)
            dAngleX += angle;
        if (y_axis)
            dAngleY += angle;
        if (z_axis)
            dAngleZ += angle;
        angle = 0;
        if (checkBox.checked) {
            if ($("#ccw")[0].checked)
                direction = 1;
            else if ($("#cw")[0].checked)
                direction = -1;

            x_axis = $("#x_axis")[0].checked;
            y_axis = $("#y_axis")[0].checked;
            z_axis = $("#z_axis")[0].checked;
        }
        else {
            direction = 0;
            x_axis = y_axis = z_axis = false;
        }
    }

    activateRotation();

    $("#rotation").click(() => {
        activateRotation();
    });

    $("#x_axis, #y_axis, #z_axis").on("change", (e) => {
        //dAxis = vec3(axis);
        
        if (x_axis)
            dAngleX += angle;
        if (y_axis)
            dAngleY += angle;
        if (z_axis)
            dAngleZ += angle;

        x_axis = $("#x_axis")[0].checked;
        y_axis = $("#y_axis")[0].checked;
        z_axis = $("#z_axis")[0].checked;

        if (dAngleX > 2*Math.PI)
            dAngleX -= 2*Math.PI;

        if (dAngleY > 2*Math.PI)
            dAngleY -= 2*Math.PI;

        if (dAngleZ > 2*Math.PI)
            dAngleZ -= 2*Math.PI;
       
        angle = 0;
    });

    //direction = 1; -> ccw
    //direction = -1; -> cw
    $("#ccw").on("change", () => {
        if ($("#ccw")[0].checked)
            direction = 1;
    });

    $("#cw").on("change", () => {
        if ($("#cw")[0].checked)
            direction = -1;
    });

    $("#speed_range").on("input", () => {
        speed = $("#speed_range")[0].value / 100;
    });

    $("#orthogonal").on("change", () => {
        if ($("#orthogonal")[0].checked)
            isPerspective = 0.0;
        else
            isPerspective = 1.0;
    });

    $("#perspective").on("change", () => {
        if ($("#perspective")[0].checked)
            isPerspective = 1.0;
        else
            isPerspective = 0.0;
    });


});