var vshaderSource = `
    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;
    uniform vec4 u_Color;
    uniform mat3 normalMatrix;
    uniform vec4 lightPosition;

    attribute vec4 a_Position;
    attribute vec3 a_Normal;

    varying vec4 color;
    void main() 
    {
      float ambientFactor = 0.3;
      vec3 lightDirection = normalize((view * lightPosition - view * model * a_Position).xyz);
      //vec3 normal = (view * model * vec4(a_Normal, 0.0)).xyz;
      vec3 normal = normalize(normalMatrix * a_Normal);
      float diffuseFactor = max(0.0, dot(lightDirection, normal));
      color = u_Color * diffuseFactor + u_Color * ambientFactor;
      color.a = 1.0;
      gl_Position = projection * view * model * a_Position;
    }
`;

var fshaderSource = `
    precision mediump float;
    varying vec4 color;
    void main() 
    {
      gl_FragColor = color;
    }
`;

var aspectRatio = 16/9.0;
var canvas;

//
// Hierarchical object using a matrix stack.
//

// A very basic stack class.
function Stack()
{
    this.elements = [];
    this.t = 0;
}

Stack.prototype.push = function(m)
{
    this.elements[this.t++] = m;
}

Stack.prototype.top = function()
{
    if (this.t <= 0)
    {
        console.log("top = ", this.t);
        console.log("Warning: stack underflow");
    } else {
        return this.elements[this.t - 1];
    }
}

Stack.prototype.pop = function()
{
    if (this.t <= 0)
    {
        console.log("Warning: stack underflow");
    }
    else
    {
        this.t--;
        var temp = this.elements[this.t];
        this.elements[this.t] = undefined;
        return temp;
    }
}

Stack.prototype.isEmpty = function()
{
    return this.t <= 0;
}


// Creates data for vertices, colors, and normal vectors for
// a unit cube.  Return value is an object with three attributes
// vertices, colors, and normals, each referring to a Float32Array.
// (Note this is a "self-invoking" anonymous function.)
var cube = function makeCube()
{
    // vertices of cube
    var rawVertices = new Float32Array([  
    -0.5, -0.5, 0.5,
    0.5, -0.5, 0.5,
    0.5, 0.5, 0.5,
    -0.5, 0.5, 0.5,
    -0.5, -0.5, -0.5,
    0.5, -0.5, -0.5,
    0.5, 0.5, -0.5,
    -0.5, 0.5, -0.5]);

    var rawColors = new Float32Array([
    1.0, 0.0, 0.0, 1.0,  // red
    0.0, 1.0, 0.0, 1.0,  // green
    0.0, 0.0, 1.0, 1.0,  // blue
    1.0, 1.0, 0.0, 1.0,  // yellow
    1.0, 0.0, 1.0, 1.0,  // magenta
    0.0, 1.0, 1.0, 1.0,  // cyan
    ]);

    var rawNormals = new Float32Array([
    0, 0, 1,
    1, 0, 0,
    0, 0, -1,
    -1, 0, 0,
    0, 1, 0,
    0, -1, 0 ]);


    var indices = new Uint16Array([
    0, 1, 2, 0, 2, 3,  // z face
    1, 5, 6, 1, 6, 2,  // +x face
    5, 4, 7, 5, 7, 6,  // -z face
    4, 0, 3, 4, 3, 7,  // -x face
    3, 2, 6, 3, 6, 7,  // + y face
    4, 5, 1, 4, 1, 0   // -y face
    ]);
    
    var verticesArray = [];
    var colorsArray = [];
    var normalsArray = [];
    for (var i = 0; i < 36; ++i)
    {
        // for each of the 36 vertices...
        var face = Math.floor(i / 6);
        var index = indices[i];
        
        // (x, y, z): three numbers for each point
        for (var j = 0; j < 3; ++j)
        {
            verticesArray.push(rawVertices[3 * index + j]);
        }
        
        // (r, g, b, a): four numbers for each point
        for (var j = 0; j < 4; ++j)
        {
            colorsArray.push(rawColors[4 * face + j]);
        }
        
        // three numbers for each point
        for (var j = 0; j < 3; ++j)
        {
            normalsArray.push(rawNormals[3 * face + j]);
        }
    }
    
    return {
        numVertices: 36,
        vertices: new Float32Array(verticesArray),
        colors: new Float32Array(colorsArray),
        normals: new Float32Array(normalsArray)
    };
}();


function makeNormalMatrixElements(model, view)
{
    var n = new Matrix4(view).multiply(model);
    n.transpose();
    n.invert();
    n = n.elements;
    return new Float32Array([
    n[0], n[1], n[2],
    n[4], n[5], n[6],
    n[8], n[9], n[10] ]);
}


// A few global variables...

// the OpenGL context
var gl;

// handle to a buffer on the GPU
var vertexBuffer;
var vertexNormalBuffer;

// handle to the compiled shader program on the GPU
var lightingShader;

// transformation matrices defining 5 objects in the scene
var torsoMatrix = new Matrix4().setTranslate(0,0,0);
var leftShoulderMatrix = new Matrix4().setTranslate(6.5, 2, 0);
var rightShoulderMatrix = new Matrix4().setTranslate(-6.5, 2, 0);
var leftArmMatrix = new Matrix4().setTranslate(0, -5, 0);
var rightArmMatrix = new Matrix4().setTranslate(0, -5, 0);
var leftHandMatrix = new Matrix4().setTranslate(0, -4, 0);
var rightHandMatrix = new Matrix4().setTranslate(0, -4, 0);
var headMatrix = new Matrix4().setTranslate(0, 7, 0);
var leftLegMatrix = new Matrix4().setTranslate(2, -10, 0);
var leftShinMatrix = new Matrix4().setTranslate(0, -6, 0);
var leftFootMatrix = new Matrix4().setTranslate(0, -3, 1.5);
var rightLegMatrix = new Matrix4().setTranslate(-2, -10, 0);
var rightShinMatrix = new Matrix4().setTranslate(0, -6, 0);
var rightFootMatrix = new Matrix4().setTranslate(0, -3, 1.5);

var torsoAngle = 0.0;
var leftShoulderAngle = 0.0;
var rightShoulderAngle = 0.0;
var leftArmAngle = 0.0;
var rightArmAngle = 0.0;
var leftHandAngle = 0.0;
var rightHandAngle = 0.0;
var headAngle = 0.0;
var leftLegAngle = 0.0;
var leftShinAngle = 0.0;
var leftFootAngle = 0.0;
var rightLegAngle = 0.0;
var rightShinAngle = 0.0;
var rightFootAngle = 0.0;

var torsoMatrixLocal = new Matrix4().setScale(10, 10, 5);
var leftShoulderMatrixLocal = new Matrix4().setScale(3, 5, 2);
var rightShoulderMatrixLocal = new Matrix4().setScale(3, 5, 2);
var leftArmMatrixLocal = new Matrix4().setScale(3, 5, 2);
var rightArmMatrixLocal = new Matrix4().setScale(3, 5, 2);
var leftHandMatrixLocal = new Matrix4().setScale(1, 3, 3);
var rightHandMatrixLocal = new Matrix4().setScale(1, 3, 3);
var headMatrixLocal = new Matrix4().setScale(4, 4, 4);
var leftLegMatrixLocal = new Matrix4().setScale(3, 7, 3);
var leftShinMatrixLocal = new Matrix4().setScale(3, 5, 3);
var leftFootMatrixLocal = new Matrix4().setScale(3, 2, 6);
var rightLegMatrixLocal = new Matrix4().setScale(3, 7, 3);
var rightShinMatrixLocal = new Matrix4().setScale(3, 5, 3);
var rightFootMatrixLocal = new Matrix4().setScale(3, 2, 6);

// view matrix
var view = new Matrix4().setLookAt(
        -20, 25, 20,   // eye
        0, 0, 0,      // at - looking at the origin
        0, 1, 0);     // up vector - y axis

var projection = new Matrix4().setPerspective(45, aspectRatio, 0.1, 1000);

// translate keypress events to strings
// from http://javascript.info/tutorial/keyboard-events
function getChar(event) {
    if (event.which == null) {
        return String.fromCharCode(event.keyCode) // IE
    } else if (event.which!=0 && event.charCode!=0) {
        return String.fromCharCode(event.which)   // the rest
    } else {
        return null // special key
    }
}

// handler for key press events adjusts object rotations
function handleKeyPress(event)
{
    var ch = getChar(event);
    switch(ch)
    {
    case 't':
        torsoAngle += 15;
        torsoMatrix.setTranslate(0,0,0).rotate(torsoAngle, 0, 1, 0);
        break;
    case 'T':
        torsoAngle -= 15;
        torsoMatrix.setTranslate(0,0,0).rotate(torsoAngle, 0, 1, 0);
        break;
    case 's':
        leftShoulderAngle += 15;
        // rotate shoulder clockwise about a point 2 units above its center
        var currentShoulderRot = new Matrix4().setTranslate(0, 2, 0).rotate(-leftShoulderAngle, 1, 0, 0).translate(0, -2, 0);
        leftShoulderMatrix.setTranslate(6.5, 2, 0).multiply(currentShoulderRot);
        break;
    case 'S':
        leftShoulderAngle -= 15;
        var currentShoulderRot = new Matrix4().setTranslate(0, 2, 0).rotate(-leftShoulderAngle, 1, 0, 0).translate(0, -2, 0);
        leftShoulderMatrix.setTranslate(6.5, 2, 0).multiply(currentShoulderRot);
        break;
    case 'd':
        rightShoulderAngle += 15;
        // rotate shoulder clockwise about a point 2 units above its center
        var currentRightShoulderRot = new Matrix4().setTranslate(0, 2, 0).rotate(-rightShoulderAngle, 1, 0, 0).translate(0, -2, 0);
        rightShoulderMatrix.setTranslate(-6.5, 2, 0).multiply(currentRightShoulderRot);
        break;
    case 'D':
        rightShoulderAngle -= 15;
        var currentRightShoulderRot = new Matrix4().setTranslate(0, 2, 0).rotate(-rightShoulderAngle, 1, 0, 0).translate(0, -2, 0);
        rightShoulderMatrix.setTranslate(-6.5, 2, 0).multiply(currentRightShoulderRot);
        break;       
    case 'a':
        leftArmAngle += 15;
        // rotate leftArm clockwise about its top front corner
        var currentArm = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-leftArmAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        leftArmMatrix.setTranslate(0, -5, 0).multiply(currentArm);
        break;
    case 'A':
        leftArmAngle -= 15;
        var currentArm = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-leftArmAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        leftArmMatrix.setTranslate(0, -5, 0).multiply(currentArm);
        break;
    case 'q':
        rightArmAngle += 15;
        // rotate leftArm clockwise about its top front corner
        var currentArmRight = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-rightArmAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        rightArmMatrix.setTranslate(0, -5, 0).multiply(currentArmRight);
        break;
    case 'Q':
        rightArmAngle -= 15;
        var currentArmRight = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-rightArmAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        rightArmMatrix.setTranslate(0, -5, 0).multiply(currentArmRight);
        break;        
    case 'h':
        leftHandAngle += 15;
        leftHandMatrix.setTranslate(0, -4, 0).rotate(leftHandAngle, 0, 1, 0);
        break;
    case 'H':
        leftHandAngle -= 15;
        leftHandMatrix.setTranslate(0, -4, 0).rotate(leftHandAngle, 0, 1, 0);
        break;
    case 'j':
        rightHandAngle += 15;
        rightHandMatrix.setTranslate(0, -4, 0).rotate(rightHandAngle, 0, 1, 0);
        break;
    case 'J':
        rightHandAngle -= 15;
        rightHandMatrix.setTranslate(0, -4, 0).rotate(rightHandAngle, 0, 1, 0);
        break;

    case 'u':
        leftLegAngle += 15;      
        var currentLeftLegMatrix = new Matrix4().setTranslate(0, 4, 1.0).rotate(-leftLegAngle, 1, 0, 0).translate(0, -4, -1.0);
        leftLegMatrix.setTranslate(2, -10, 0).multiply(currentLeftLegMatrix);
        break;
    case 'U':
        leftLegAngle -= 15;
        var currentLeftLegMatrix = new Matrix4().setTranslate(0, 4, 1.0).rotate(-leftLegAngle, 1, 0, 0).translate(0, -4, -1.0);
        leftLegMatrix.setTranslate(2, -10, 0).multiply(currentLeftLegMatrix);
        break;
    case 'i':
        leftShinAngle += 15;      
        var currentLeftShinMatrix = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-leftShinAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        leftShinMatrix.setTranslate(0, -6, 0).multiply(currentLeftShinMatrix);
        break;
    case 'I':
        leftShinAngle -= 15;
        var currentLeftShinMatrix = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-leftShinAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        leftShinMatrix.setTranslate(0, -6, 0).multiply(currentLeftShinMatrix);
        break;        
    case 'o':
        leftFootAngle += 15;      
        var currentLeftFootMatrix = new Matrix4().setTranslate(-2.5, 0, 0).rotate(-leftFootAngle, 1, 0, 0).translate(2.5, 0, 0);
        leftFootMatrix.setTranslate(0, -3, 1.5).multiply(currentLeftFootMatrix);
        break;
    case 'O':
        leftFootAngle -= 15;
        var currentLeftFootMatrix = new Matrix4().setTranslate(-2.5, 0, 0).rotate(-leftFootAngle, 1, 0, 0).translate(2.5, 0, 0);
        leftFootMatrix.setTranslate(0, -3, 1.5).multiply(currentLeftFootMatrix);
        break;

    case 'p':
        rightLegAngle += 15;      
        var currentRightLegMatrix = new Matrix4().setTranslate(0, 4, 1.0).rotate(-rightLegAngle, 1, 0, 0).translate(0, -4, -1.0);
        rightLegMatrix.setTranslate(-2, -10, 0).multiply(currentRightLegMatrix);
        break;
    case 'P':
        rightLegAngle -= 15;
        var currentRightLegMatrix = new Matrix4().setTranslate(0, 4, 1.0).rotate(-rightLegAngle, 1, 0, 0).translate(0, -4, -1.0);
        rightLegMatrix.setTranslate(-2, -10, 0).multiply(currentRightLegMatrix);
        break;
    case 'n':
        rightShinAngle += 15;      
        var currentRightShinMatrix = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-rightShinAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        rightShinMatrix.setTranslate(0, -6, 0).multiply(currentRightShinMatrix);
        break;
    case 'N':
        rightShinAngle -= 15;
        var currentRightShinMatrix = new Matrix4().setTranslate(0, 2.5, 1.0).rotate(-rightShinAngle, 1, 0, 0).translate(0, -2.5, -1.0);
        rightShinMatrix.setTranslate(0, -6, 0).multiply(currentRightShinMatrix);
        break;        
    case 'm':
        rightFootAngle += 15;      
        var currentRightFootMatrix = new Matrix4().setTranslate(-2.5, 0, 0).rotate(-rightFootAngle, 1, 0, 0).translate(2.5, 0, 0);
        rightFootMatrix.setTranslate(0, -3, 1.5).multiply(currentRightFootMatrix);
        break;
    case 'M':
        rightFootAngle -= 15;
        var currentRightFootMatrix = new Matrix4().setTranslate(-2.5, 0, 0).rotate(-rightFootAngle, 1, 0, 0).translate(2.5, 0, 0);
        rightFootMatrix.setTranslate(0, -3, 1.5).multiply(currentRightFootMatrix);
        break;        

    case 'l':
        headAngle += 15;
        headMatrix.setTranslate(0, 7, 0).rotate(headAngle, 0, 1, 0);
        break;
    case 'L':
        headAngle -= 15;
        headMatrix.setTranslate(0, 7, 0).rotate(headAngle, 0, 1, 0);
        break;
        default:
    return;
    }
}

// helper function renders the cube based on the model transformation
// on top of the stack and the given local transformation
function renderCube(matrixStack, matrixLocal)
{
      // bind the shader
      gl.useProgram(lightingShader);

      // get the index for the a_Position attribute defined in the vertex shader
      var positionIndex = gl.getAttribLocation(lightingShader, 'a_Position');
      if (positionIndex < 0) {
        console.log('Failed to get the storage location of a_Position');
        return;
      }

      var normalIndex = gl.getAttribLocation(lightingShader, 'a_Normal');
      if (normalIndex < 0) {
        console.log('Failed to get the storage location of a_Normal');
        return;
      }
     
      // "enable" the a_position attribute 
      gl.enableVertexAttribArray(positionIndex);
      gl.enableVertexAttribArray(normalIndex);
     
      // bind data for points and normals
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
      gl.vertexAttribPointer(positionIndex, 3, gl.FLOAT, false, 0, 0);
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexNormalBuffer);
      gl.vertexAttribPointer(normalIndex, 3, gl.FLOAT, false, 0, 0);
      
      var loc = gl.getUniformLocation(lightingShader, "view");
      gl.uniformMatrix4fv(loc, false, view.elements);
      loc = gl.getUniformLocation(lightingShader, "projection");
      gl.uniformMatrix4fv(loc, false, projection.elements);
      loc = gl.getUniformLocation(lightingShader, "u_Color");
      gl.uniform4f(loc, 0.0, 1.0, 0.0, 1.0);
      var loc = gl.getUniformLocation(lightingShader, "lightPosition");
      gl.uniform4f(loc, 5.0, 10.0, 5.0, 1.0);
    
      var modelMatrixloc = gl.getUniformLocation(lightingShader, "model");
      var normalMatrixLoc = gl.getUniformLocation(lightingShader, "normalMatrix");
      
      // transform using current model matrix on top of stack
      var current = new Matrix4(matrixStack.top()).multiply(matrixLocal);
      gl.uniformMatrix4fv(modelMatrixloc, false, current.elements);
      gl.uniformMatrix3fv(normalMatrixLoc, false, makeNormalMatrixElements(current, view))
      
      gl.drawArrays(gl.TRIANGLES, 0, 36);
    
      // on safari 10, buffer cannot be disposed before drawing...  
      gl.bindBuffer(gl.ARRAY_BUFFER, null);
      gl.useProgram(null);
}

// code to actually render our geometry
function draw()
{
    // clear the framebuffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BIT);

    // set up the matrix stack  
    var s = new Stack();
    s.push(torsoMatrix);
    renderCube(s, torsoMatrixLocal);
    
    // right shoulder relative to torso
    s.push (new Matrix4(s.top()).multiply(rightShoulderMatrix)); 
    renderCube(s, rightShoulderMatrixLocal);
        
    // rightArm relative to shoulder
    s.push(new Matrix4(s.top()).multiply(rightArmMatrix));
    renderCube(s, rightArmMatrixLocal);

    s.push(new Matrix4(s.top()).multiply(rightHandMatrix));
    renderCube(s, rightHandMatrixLocal);
    s.pop();
    s.pop();
    s.pop();

    // left shoulder relative to torso
    s.push (new Matrix4(s.top()).multiply(leftShoulderMatrix)); 
    renderCube(s, leftShoulderMatrixLocal);
      
    // leftArm relative to shoulder
    s.push(new Matrix4(s.top()).multiply(leftArmMatrix));
    renderCube(s, leftArmMatrixLocal);
        
    // leftHand relative to arm
    s.push(new Matrix4(s.top()).multiply(leftHandMatrix));
    renderCube(s, leftHandMatrixLocal);
    s.pop();
    s.pop();
    s.pop();

    s.push(new Matrix4(s.top()).multiply(leftLegMatrix));
    renderCube(s, leftLegMatrixLocal);

    s.push(new Matrix4(s.top()).multiply(leftShinMatrix));
    renderCube(s, leftShinMatrixLocal);

    s.push(new Matrix4(s.top()).multiply(leftFootMatrix));
    renderCube(s, leftFootMatrixLocal);

    s.pop();    
    s.pop();
    s.pop();

    s.push(new Matrix4(s.top()).multiply(rightLegMatrix));
    renderCube(s, rightLegMatrixLocal);

    s.push(new Matrix4(s.top()).multiply(rightShinMatrix));
    renderCube(s, rightShinMatrixLocal);

    s.push(new Matrix4(s.top()).multiply(rightFootMatrix));
    renderCube(s, rightFootMatrixLocal);

    s.pop();    
    s.pop();
    s.pop();        
    
    // head relative to torso
    s.push(new Matrix4(s.top()).multiply(headMatrix));
    renderCube(s, headMatrixLocal);
    s.pop();
    s.pop();

    if (!s.isEmpty())
    {
        console.log("Warning: pops do not match pushes");
    }

}

function updateCanvasSize (gl) {
    let percentage;
    aspectRatio = 16/9.0;
    if (window.innerWidth >= 1200)
        percentage = 0.6;
    else if (window.innerWidth > 768)
        percentage = 0.86;
    else if (window.innerWidth <= 768) {
        percentage = 0.80;
        aspectRatio = 1;
    }

    projection = new Matrix4().setPerspective(45, aspectRatio, 0.1, 1000);
    canvas.width = (window.innerWidth - 2) * percentage; // subtracting the borders
    canvas.height = canvas.width / aspectRatio;
    gl.viewport(0, 0, canvas.width, canvas.height);
    $("#form-container").css("width", percentage * 100 + "%");
}

// entry point when page is loaded
window.onload = function main() {

  // basically this function does setup that "should" only have to be done once,
  // while draw() does things that have to be repeated each time the canvas is 
  // redrawn    
    
  // retrieve <canvas> element
  canvas = document.getElementById('main-canvas');

  // key handler
  window.onkeypress = handleKeyPress;

  // get the rendering context for WebGL, using the utility from the teal book
  gl = getWebGLContext(canvas);
  updateCanvasSize(gl);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }
  
  if (!initShaders(gl, vshaderSource, fshaderSource)) {
    console.log('Failed to intialize shaders.');
    return;
  }
  lightingShader = gl.program;
  gl.useProgram(null);
  
  // buffer for vertex positions for triangles
  vertexBuffer = gl.createBuffer();
  if (!vertexBuffer) {
      console.log('Failed to create the buffer object');
      return;
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, cube.vertices, gl.STATIC_DRAW);

  // buffer for vertex normals
  vertexNormalBuffer = gl.createBuffer();
  if (!vertexNormalBuffer) {
      console.log('Failed to create the buffer object');
      return;
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexNormalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, cube.normals, gl.STATIC_DRAW);
  
  // buffer is not needed anymore (not necessary, really)
  gl.bindBuffer(gl.ARRAY_BUFFER, null); 

  // specify a fill color for clearing the framebuffer
  gl.clearColor(0.9, 0.9, 0.9, 1.0);
  
  gl.enable(gl.DEPTH_TEST);
  
  // define an animation loop
  var animate = function() {
    draw();
    requestAnimationFrame(animate, canvas); 
  };
  
  // start drawing!
  animate();
}

window.addEventListener("resize", e => {
    updateCanvasSize(gl);
});