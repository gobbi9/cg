/**
 * Class that represents a regular shape with n sides. If n is big the shape will look like a circle/circumference.
 * @inherits {Shape}
 */
class RegularShape extends Shape {
	/**
	 * Constructor of RegularShape
	 * @param  {array} centroid - an array of 2,3 or 4-component that represents the centroid of the shape
	 * @param  {float} radius - the size of the shape's radius.
	 * @param  {int} sides - how many sides the shape has.
	 * @param  {array} color - fill color of the shape (an array with 4 components in the format [red, green, blue, alpha]. Each of the values varies between 0 and 1 included). If the fill color is set as undefined or not given as parameter, only the polygon's outline will be drawn.
	 * @param  {float} borderSize - a number greater than zero, that represents the thickness of the border.
	 * @param  {array} borderColor - the border color (same type of color)
	 */	
	constructor(centroid, radius, sides, color, borderSize = 0, borderColor = [0.3, 0.3, 0.3, 1.0]) {
		super(regularPoints(centroid, radius, sides), color, borderSize, borderColor);
		this.radius = radius;
		this.sides = sides;
	}

	set regularPoints(centroid) {
		this.points = regularPoints(centroid, radius, sides);
	}
}