/**
 * @param  {array} points - a list of lists/points, example: [[1,2],[3,4]]
 * @return {array} - a flattened array, example: [1,2,3,4]
 */
function flattenPoints(points){
	let flattenedPoints = [];
	for (let point of points)
		for (let coord of point)
			flattenedPoints.push(coord);
	return flattenedPoints;
}

//http://stackoverflow.com/questions/9960908/permutations-in-javascript/37580979#37580979
function* permute(permutation) {
  var length = permutation.length,
      c = Array(length).fill(0),
      i = 1;

  yield permutation;
  while (i < length) {
    if (c[i] < i) {
      var k = (i % 2) ? c[i] : 0,
          p = permutation[i];
      permutation[i] = permutation[k];
      permutation[k] = p;
      ++c[i];
      i = 1;
      yield permutation;
    } else {
      c[i] = 0;
      ++i;
    }
  }
}

/**
 * A stub webgl context created to enable the access to constants, without having to pass the context as parameters to classes like {@link Shape}.
 */
var STUB_CONTEXT = document.createElement("canvas").getContext("webgl");

/**
 * @param  {array} point   
 * @param  {array} pointList
 * @return {array}   Given a point x and a list of points L where x is not in L, this function will return an array where x is the first element and the other elements are the two points closer to x.
 */
function getNeighbors(point, pointList) {
	let neighbors = [];
	neighbors.push(point);
	let pointListCopy = pointList.slice();
	pointListCopy.sort((a,b) => {
		return length(subtract(point, a)) - length(subtract(point, b));
	});

	neighbors.push(pointListCopy[0], pointListCopy[1]);
	//neighbors.concat(pointListCopy);
	return neighbors;
}

/**
 * @param  {array}  point
 * @param  {array}  listPoint
 * @return {boolean}  returns true if point is inside listPoint, false if otherwise.
 */
function hasPoint(point, listPoint){
	for (let p of listPoint){
		let i = 0;
		for (let coord of p) {
			if (coord != point[i])
				break;
			i++;
			if (i == p.length)
				return true;
		}
	}
	return false;
}

function tesselate(points, count = 1, middle = 0.5, fill = false) {
	var outPoints = [];
	tesselate_rec(points, count, middle, outPoints, fill);
	return outPoints;
}

/**
 * @param  {array} points - an array of coordinates
 * @param  {int} count - the depth of the recursion i.e. number of subdivisions
 * @param  {float} middle - a number between 0.0 and 1.0 indicating where in the lines the subdivision should occur. middle = 0.5 means in the center.
 * @param  {array} outPoints - array that stores all points of the resulting fractal
 */
function tesselate_rec(points, count, middle, outPoints, fill) {
	// check for end of recursion
	if ( count === 0 ) {
		for (let point of points)
			//if (!hasPoint(point, outPoints))
				outPoints.push(point);
	}
	else {
		let intersections = [];

		let i = 0;
		for (let point of points){
			let neighbors = getNeighbors(point, points.slice(0, i).concat(points.slice(i+1, points.length)));
			neighbors = neighbors.slice(1, neighbors.length);
			for (let neighbor of neighbors){
				let intersection = mix(point, neighbor, middle);
				if (!hasPoint(intersection, intersections)){
					intersections.push(intersection);
				}
			}
			i++;	
		}

		if (fill && intersections.length == 3) {
			tesselate_rec(intersections, count-1, middle, outPoints, fill);
		}

		for (let point of points) 
			tesselate_rec(getNeighbors(point, intersections), count-1, middle, outPoints, fill);

	}
}

$.fn.extend({
    disableSelection: function() {
        this.each(function() {
            this.onselectstart = function() {
                return false;
            };
            this.unselectable = "on";
            $(this).css('-moz-user-select', 'none');
            $(this).css('-webkit-user-select', 'none');
        });
        return this;
    }
});


function regularPoints(centroid, radius, sides) {
	let dTheta = 2*Math.PI / sides;
	let theta = 0;
	let points = [];
	let x0 = centroid[0], y0 = centroid[1];
	points.push([x0, y0].concat([0.0, 1.0]));

	for (let i = 0; i < sides; i++){
		points.push([x0 + radius * Math.cos(theta), y0 + radius * Math.sin(theta), 0.0, 1.0]);
		theta += dTheta;
	}

	points.push(points[1]);

	return points;	
}